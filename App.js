import React from 'react';
import { NativeModules, Button, View, PermissionsAndroid } from 'react-native';

const { NearbyConnection } = NativeModules;

const App = () => {
    const discover = () => {
        NearbyConnection.startDiscovery(
            "ritik",
            "12345",
        );
        console.log(NearbyConnection.isDiscovering());
    }

    const advertise = () => {
        NearbyConnection.startAdvertising(
            "ritik",
            "12345",
        );
        console.log(NearbyConnection.isAdvertising());
    }
    return(
        <View>
            <Button 
                title="Advertise"
                color='#841584'
                onPress={advertise}
            />
            <Button 
                title="Discover"
                color='#841584'
                onPress={discover}
            />
        </View>
    )
}

export default App;